# Mein verteiltes Datenbackup

Dieses Repository enthält eine Sammlung von Daten, mit denen ich mein
privates, verteiltes Backup-of-Last-Resort erstelle.

In dieses Backup kommen bei mir PGP-Keys, SSH-Keys (und natürlich die
Passworter dazu), ein Password-Store, Instant-Messenger-Identitäten,
eine SSH-Config (die die relevanten Hosts enthält) und weitere
Dateien (z.B. Kontoauszüge, Steuerdaten, ...).

## Ziele

Mit diesem Backup möchte ich zwei Probleme lösen:

Wiederherstellen meines Backups:
Natürlich habe ich ein Backup meiner relevanten Daten. Und das habe ich
nicht nur an einer Stelle, sondern off "off-site".
Aber: Das Backup ist verschlüsselt (also brauche ich den passenden 
Key/Passwort) und off-site muss ich dann auch noch per SSH oder ähnliches
hin.
Um im Fall eines totalen Datenverlustes (Handy weg, Laptop weg) trotzdem an die
Daten zu gelangen habe ich die Keys in diesem Backup.

Jemand anderes muss an meine Daten, wenn ich nicht mehr kann:
Vielleicht kommt irgendwann ein Zeitpunkt, an dem ich nicht mehr auf meine
Daten zugreifen kann - warum auch immer.
In diesem Fall möchte ich anderen ermöglichen an meine E-Mails, Bankkonten,
Messenger und ähnliches zu kommen.

Dabei möchte ich nicht einer einzelnen Person vertrauen müssen.
Nicht, weil ich dieser Person nicht ausreichend vertraue.
Sondern weil ich der Person nicht die Bürde auferlegen möchte allein
für die Sicherheit meiner Daten verantwortlich zu sein.

Außerdem möchte ich vor dem Ausfall eines Teilnehmers des Backups
geschützt sein.

## Technik

In diesem System erstelle ich drei ähnliche Backups, die ich an drei
Personen verteile.
Jede vertrauenswürde Person bekommt eine komplette Kopie
der Daten, die mit jeweils einem Passwort verschlüsselt ist.
Die beiden anderen Personen bekommen jeweile die zwei anderen Passwörter.

Tun sich nun zwei oder mehr Personen zusammen können die an die Daten gelangen.
Neben dem Backup und dem Passwörtern der anderen Personen bekommt jede Person
auch die Kontaktdaten der anderen.
So muss ich nicht voraussetzen, dass die Personen kontakt halten.


## Umsetzung

* Die Datei `README_FIRST.txt.template` zu `README_FIRST.txt` kopieren und
  die Felder ausfüllen.
* Die Daten, die Du sichern möchtest in den Ordner `data/`
  legen.
* `./create.sh` packt `data/` als Zip zusammen und verchlüsselt die
  Archive dan mit `gpg2` und einem symmetrischen (Passwort-) Verfahren.
* `./create.sh` legt in `packages/` die Pakete für die jeweils drei Personen
  für Dich an.
* Die Pakete aus `packages/` musst Du jetzt nur noch drei Personen zukommen
  lassen.
