#!/bin/bash

# generate passwords for the packs
PWDA=$(pwgen 20 1)
PWDB=$(pwgen 20 1)
PWDC=$(pwgen 20 1)

# create inner zip
zip -r -0 backup.zip data/

# encrypt packages
rm -rf packages/
mkdir -p packages/for_a/
mkdir -p packages/for_b/
mkdir -p packages/for_c/
gpg2 --batch --passphrase "$PWDA" --output packages/for_a/encrypted.zip.gpg -c backup.zip
gpg2 --batch --passphrase "$PWDB" --output packages/for_b/encrypted.zip.gpg -c backup.zip
gpg2 --batch --passphrase "$PWDC" --output packages/for_c/encrypted.zip.gpg -c backup.zip

# write passwords
echo Die Passwörter für die anderen Sicherungen sind > packages/for_a/passwoerter.txt
echo "$PWDB" >> packages/for_a/passwoerter.txt
echo "$PWDC" >> packages/for_a/passwoerter.txt

echo Die Passwörter für die anderen Sicherungen sind > packages/for_b/passwoerter.txt
echo "$PWDA" >> packages/for_b/passwoerter.txt
echo "$PWDC" >> packages/for_b/passwoerter.txt

echo Die Passwörter für die anderen Sicherungen sind > packages/for_c/passwoerter.txt
echo "$PWDB" >> packages/for_c/passwoerter.txt
echo "$PWDA" >> packages/for_c/passwoerter.txt

# add info
cp README_FIRST.txt packages/for_a/
cp README_FIRST.txt packages/for_b/
cp README_FIRST.txt packages/for_c/

# debug output
echo Hier noch einmal alle Passwörter für eine vierte Kopie: > packages/alle_passwoerter.txt
echo Passwort für Paket A: "$PWDA" >> packages/alle_passwoerter.txt
echo Passwort für Paket B: "$PWDB" >> packages/alle_passwoerter.txt
echo Passwort für Paket C: "$PWDC" >> packages/alle_passwoerter.txt
